# [3 Ways You’ll Never Design Apps Again](https://appdev-coe.gitlab.io/3-ways-youll-never-design-apps-again)

 - [Lightning talk session for 3 Ways You’ll Never Design Apps Again](https://appdev-coe.gitlab.io/3-ways-youll-never-design-apps-again/lightning-talk.html)

 - [Full session for 3 Ways You’ll Never Design Apps Again](https://appdev-coe.gitlab.io/3-ways-youll-never-design-apps-again/full-talk.html)


## Abstract:
Did you forget who you’re working for when designing your applications? Every once in awhile it’s a good idea to take a step back and learn from our application development experiences. In this session you’ll take a look at three ways every developer stumbles again and again when designing applications. They are not obvious bumps in the road, but repeatedly we catch our toes on them. The attendee learns about fundamental issues that every developer (an/or architect) needs to be aware of to avoid design flaws that put us in the breakdown lane on the highway/motorway.

 - Who are you working for? (Ignoring the user)

 - Trip over testing

 - Foundational development approach



[![Cover Slide](https://gitlab.com/appdev-coe/3-ways-youll-never-design-apps-again/raw/master/cover.png)](https://appdev-coe.gitlab.io/3-ways-youll-never-design-apps-again)
